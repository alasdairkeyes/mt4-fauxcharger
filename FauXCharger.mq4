//+------------------------------------------------------------------+
//|                                                  FauXCharger.mq4 |
//|                                                   Alasdair Keyes |
//|                 https://gitlab.com/alasdairkeyes/mt4-fauxcharger |
//+------------------------------------------------------------------+
//
// This is a clone of the FXCharger EA as featured on
// https://www.myfxbook.com/members/fxcharger
//
// It has been created from studying the trade history on the
// MyFXBook page only and as such may not be as fully featured as
// the original.
// It implements a martingale trading strategy which is known to be
// very risky, caution should be taken.
//
// == License
//
// This is released under the GNU GPL v3. See included LICENSE file
// for full details. This is provided "as is" without warranty of any
// kind, either expressed or implied.
//
// == EA Settings
//
// The default settings are suitable for applying to the EURUSD Daily
// chart.
//
// It is recommended that the 'LotSizeMultiplier' and
// 'TakeProfitMultiplier'settings are not modified as they provide
// the Martingale functionality, however the ability to change them
// has been added so the user can adjust at their discretion.
//
// OriginalOrderLotSize
//   This is the Lot size of the first pair of orders created. Make
//   sure this is set small enough to allow further, larger lots to
//   be placed in martingale fashion.
//
// OriginalOrderTakeProfit
//   This is the size of Take Profit for the first pair of orders
//   created. This value is in Metatrader 'Points' for EU, there are
//   10 points per pip.
//
// OriginalOrderAtNewBarOnly
//   When this is set to true, the first pair of orders will only be
//   opened when a new bar starts. When set to false the EA will try
//   to open the first pair of orders as soon as possible.
//
// LotSizeMultiplier
//   When further orders are placed in a martingale fashion. The
//   subsequent orders Lot Sizes are multiplied by this value.
//
// TakeProfitMultiplier
//   When further orders are placed in a martingale fashion. The
//   subsequent orders Take Profit targets are multiplied by this
//   value.
//
// AllowedSlippage
//   The amount of slippage allowed when placing a new order
//
// KeepTakeProfitOnLargestOrderOnly
//   When a new order is created in a martingale situation, the
//   TakeProfit from any previous orders will be removed, so the
//   current run of open orders can only be ended when the largest
//   order hits profit targets.
//   This attempts to stop issues of lost profits in range-bound
//   markets by older order TakeProfit levels being hit.
//   This defaults to true.
//
// OnlyOpenTradesOnWeekdays
//   Depending on locations/timezones, dome trading servers will
//   open on Sunday evenings for a few hours, this can cause orders to
//   be places a couple of hours apart when Monday arrives shortly after
//   This setting can be used to define if trades should be opened
//   Monday-Friday.
//   This defaults to true.
//
// == Currency Pairs and settings
// 
// See the README.md file for some suggestions

#define NAME "FauXCharger"
#define VERSION "1.04"
#define MAGICMA 245768

#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-fauxcharger"
#property version   VERSION
#property strict

#include <stderror.mqh>
#include <stdlib.mqh>

// User Modifiable variables
input double   OriginalOrderLotSize = 0.01;
input int      OriginalOrderTakeProfit = 1000;
input bool     OriginalOrderAtNewBarOnly = true;

input double   LotSizeMultiplier = 4.0;
input double   TakeProfitMultiplier = 0.5;
input int      AllowedSlippage = 3;

input bool     KeepTakeProfitOnLargestOrderOnly = true;

input bool     OnlyOpenTradesOnWeekdays = true;

// Internal Globals
datetime RunThisBarTime;
int      PreviousBarLongOrderCount;
int      PreviousBarShortOrderCount;
bool     FirstTickHasRun = false;

/*
 * Startup
 */
int OnInit()
{
   return(INIT_SUCCEEDED);
}

/*
 * Tear down
 */
void OnDeinit(const int reason)
{
}

/*
 * If the EA is in a martingale situation, check for a drop in Open orders to
 * indicate a TP has been hit and then close out all other orders
 */
bool CheckForClosure(int OpenLongOrderCount, int OpenShortOrderCount)
{
   bool CloseOrders = false;

   if (PreviousBarShortOrderCount == 0 && PreviousBarLongOrderCount > OpenLongOrderCount)
      CloseOrders = true;

   if (PreviousBarLongOrderCount == 0 && PreviousBarShortOrderCount > OpenShortOrderCount)
      CloseOrders = true;

   // Orders need to be closed
   if (CloseOrders == true) {
      PrintFormat(
         "Number of orders has dropped Long:%d->%d Short:%d->%d, assuming TP is hit, closing out remaining orders",
         PreviousBarLongOrderCount,
         OpenLongOrderCount,
         PreviousBarShortOrderCount,
         OpenShortOrderCount
      );
      CloseAllOpenOrders();

      // Update Previous bar counts for next tick
      UpdatePreviousOrderCounts(GetOpenOrders(OP_BUY), GetOpenOrders(OP_SELL));
      return true;
   }

   return false;
}

/*
 * Tick handler
 */
void OnTick()
{
   int OpenLongOrderCount = GetOpenOrders(OP_BUY);
   int OpenShortOrderCount = GetOpenOrders(OP_SELL);

   // Check if this is the first tick after loading
   if (FirstTickHasRun == false) {
      // We have no previous information, set to existing values and wait for the next bar
      UpdatePreviousOrderCounts(OpenLongOrderCount, OpenShortOrderCount);
      PrintFormat(
         "Fetching order counts for first tick Long: %d Short: %d",
         PreviousBarLongOrderCount,
         PreviousBarShortOrderCount
      );
      FirstTickHasRun = true;
      return;
   }

   // Log when order counts have changed, this shouldn't be very spammy
   if (PreviousBarLongOrderCount != OpenLongOrderCount || PreviousBarShortOrderCount != OpenShortOrderCount) {
      PrintFormat(
         "Order Count Changed - Long: %d -> %d Short: %d -> %d",
         PreviousBarLongOrderCount,
         OpenLongOrderCount,
         PreviousBarShortOrderCount,
         OpenShortOrderCount
      );
   }

   // Look to close orders once TP has been hit
   if (CheckForClosure(OpenLongOrderCount, OpenShortOrderCount) == true)
      return;

   // Update Previous bar counts for next go round
   UpdatePreviousOrderCounts(OpenLongOrderCount, OpenShortOrderCount);

   // Bail if we're not allowed to trade. This is placed after the CheckForClosure() call
   // so that we can still exit existing orders on profit. We just won't open new ones
   if (IsTradeAllowed() == false)
      return;

   if (RunThisBar() == true) {
      if (!OriginalOrderAtNewBarOnly && OpenLongOrderCount == 0 && OpenShortOrderCount == 0) {
         Print("We have run this bar, but there are no orders and OriginalOrderAtNewBarOnly is false, continuing");
      } else {
         return;
      }
   }

   // If it's the weekend, don't run if user has defined weekday-only trading
   if (OnlyOpenTradesOnWeekdays == true && (DayOfWeek()==0 || DayOfWeek()==6)) {
      Print("Not trading as trading on non-weekdays is disabled");
      return;
   }

   CheckForNewOrders(OpenLongOrderCount, OpenShortOrderCount);
}

/*
 * Update the count of Long/Short orders
 */
void UpdatePreviousOrderCounts(int OpenLongOrderCount, int OpenShortOrderCount)
{
   PreviousBarLongOrderCount = OpenLongOrderCount;
   PreviousBarShortOrderCount = OpenShortOrderCount;
}

/*
 * Calculate the next lot size based on number of current open orders
 */
double GetNextLotSize(int NumberOfOpenOrders)
{
   return OriginalOrderLotSize * MathPow(LotSizeMultiplier, NumberOfOpenOrders);
}

/*
 * Calculate the next TP level based on number of current open orders
 */
double GetNextTPAmount(int NumberOfOpenOrders)
{
   return OriginalOrderTakeProfit * MathPow(TakeProfitMultiplier, NumberOfOpenOrders);
}

/*
 * Check if we have already run this bar and return true/false
 */
bool RunThisBar()
{
   // Check if we've run this bar.
   if (RunThisBarTime && Time[0] == RunThisBarTime)
      return true;

   RunThisBarTime = Time[0];

   // Check we're in the first ticks of this bar, otherwise reloading the EA could trigger a new order
   if (Volume[0] > 20)
      return true;

   return false;
}

/*
 * Return the number of open orders for the given OrderType
 */
int GetOpenOrders(int RequestedOrderType)
{
   int OrderCount = 0;

   for(int OrderIndex = 0; OrderIndex < OrdersTotal(); OrderIndex++) {
      if (OrderSelect(OrderIndex, SELECT_BY_POS, MODE_TRADES) == false)
         break;

      if(OrderMagicNumber() != MAGICMA || OrderSymbol() != Symbol())
         continue;

      if(OrderType() == RequestedOrderType)
         OrderCount++;
   }
   return OrderCount;
}

/*
 * Cycle through all Open orders created by this EA and close them
 */
void CloseAllOpenOrders()
{
   for(int OrderIndex = OrdersTotal() -1; OrderIndex >= 0; OrderIndex--) {
      if (OrderSelect(OrderIndex, SELECT_BY_POS, MODE_TRADES) == false)
         continue;

      if (OrderMagicNumber() != MAGICMA || OrderSymbol() != Symbol())
         continue;

      if(OrderType() == OP_BUY) {
         if (OrderClose(OrderTicket(), OrderLots(), Bid, AllowedSlippage, clrYellow)) {
            PrintFormat("Long Order %d closed OK", OrderTicket());
         } else {
            int ErrorCode = GetLastError();
            PrintFormat("Failed to close Order %d: %s (Error Code %d)", OrderTicket(), ErrorDescription(ErrorCode), ErrorCode);
         }
      } else if(OrderType() == OP_SELL) {
         if (OrderClose(OrderTicket(), OrderLots(), Ask, AllowedSlippage, clrYellow)) {
            PrintFormat("Short Order %d closed OK", OrderTicket());
         } else {
            int ErrorCode = GetLastError();
            PrintFormat("Failed to close Order %d: %s (Error Code %d)", OrderTicket(), ErrorDescription(ErrorCode), ErrorCode);
         }
      }
   }
}

/*
 * Cycle through all Open orders and remove Take Profit from all orders except
 * the Order with OrderTicket provided
 */
void RemoveTakeProfitOnOrdersExcept(int OrderTicketToKeep)
{
   PrintFormat("Removing TakeProfit on all orders except Ticket: %d", OrderTicketToKeep);
   for(int OrderIndex = OrdersTotal() -1; OrderIndex >= 0; OrderIndex--) {
      if (OrderSelect(OrderIndex, SELECT_BY_POS, MODE_TRADES) == false)
         continue;

      if (OrderMagicNumber() != MAGICMA || OrderSymbol() != Symbol())
         continue;

      if(OrderType() == OP_BUY || OrderType() == OP_SELL) {
         if (!OrderTakeProfit())
            continue;

         if (OrderTicket() != OrderTicketToKeep) {
            bool ModifyOK = OrderModify(
               OrderTicket(),
               OrderOpenPrice(),
               NULL,
               NULL,
               0,
               clrNONE
            );
            if (!ModifyOK) {
               int ErrorCode = GetLastError();
               PrintFormat("Failed to remove TakeProfit from Order %d: %s (Error Code %d)", OrderTicket(), ErrorDescription(ErrorCode), ErrorCode);
            }
         }
      }
   }
}

/*
 * Look at current number of open orders and determine if new orders need to be
 * opened
 */
void CheckForNewOrders(int OpenLongOrderCount, int OpenShortOrderCount)
{
   // Check state of orders
   if (OpenShortOrderCount == 1 && OpenLongOrderCount == 1) {
      Print("Original Orders are still in place. Skipping");
      return;
   }

   bool OpenNewLongOrder = false;
   bool OpenNewShortOrder = false;
   
   if (OpenLongOrderCount == 0 && OpenShortOrderCount == 0) {
      // There are no open orders, create a long and short
      OpenNewLongOrder = true;
      OpenNewShortOrder = true;
   } else if (OpenLongOrderCount > 0 && OpenShortOrderCount == 0) {
      // We are in a long martingale situation, place another long order
      OpenNewLongOrder = true;
   } else if (OpenShortOrderCount > 0 && OpenLongOrderCount == 0) {
      // We are in a short martingale situation, place another short order
      OpenNewShortOrder = true;
   } else {
      // Catch-all in case
      PrintFormat(
         "Not sure what to do, I have %d Long Orders and %d Short Orders",
         OpenLongOrderCount,
         OpenShortOrderCount
      );
   }
 
   if (OpenNewLongOrder) {
      double TakeProfit = NormalizeDouble(
         Ask + GetNextTPAmount(OpenLongOrderCount) * Point,
         Digits
      );
      double LotSize = NormalizeDouble(
         GetNextLotSize(OpenLongOrderCount),
         2
      );

      int Ticket = OrderSend(
         Symbol(),
         OP_BUY,
         LotSize,
         Ask,
         AllowedSlippage,
         NULL,
         TakeProfit,
         EAComment(),
         MAGICMA,
         0,
         clrBlue
      );
      if (Ticket < 0) {
         int ErrorCode = GetLastError();
         PrintFormat("Failed to send Long order: %s (Error Code %d)", ErrorDescription(ErrorCode), ErrorCode);
      } else if (OpenNewLongOrder != OpenNewShortOrder) {
         if (KeepTakeProfitOnLargestOrderOnly) {
            RemoveTakeProfitOnOrdersExcept(Ticket);
         }
      }
   }

   if (OpenNewShortOrder) {
      double TakeProfit = NormalizeDouble(
         Bid - GetNextTPAmount(OpenShortOrderCount) * Point,
         Digits
      );
      double LotSize = NormalizeDouble(
         GetNextLotSize(OpenShortOrderCount),
         2
      );

      int Ticket = OrderSend(
         Symbol(),
         OP_SELL,
         LotSize,
         Bid,
         AllowedSlippage,
         NULL,
         TakeProfit,
         EAComment(),
         MAGICMA,
         0,
         clrRed
      );
      if (Ticket < 0) {
         int ErrorCode = GetLastError();
         PrintFormat("Failed to send Short order: %s (Error Code %d)", ErrorDescription(ErrorCode), ErrorCode);
      } else if (OpenNewLongOrder != OpenNewShortOrder) {
         if (KeepTakeProfitOnLargestOrderOnly) {
            RemoveTakeProfitOnOrdersExcept(Ticket);
         }
      }
   }
}

/*
 * Provide useful comment for new orders
 */
string EAComment()
{
   return StringFormat(
      "%s-v%s-M%d", NAME, VERSION, Period()
   );
}
