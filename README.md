# mt4-FauXCharger

Open Source Clone of the FXCharger Metatrader4 Expert Adviser

## Outline

This is a clone of the FXCharger EA as featured on https://www.myfxbook.com/members/fxcharger. It has been created from studying the trade history on the MyFXBook page only and as such may not be as fully featured as the original. It implements a martingale trading strategy which is known to be very risky, caution should be taken.

## EA Settings

The default settings are suitable for applying to the EURUSD Daily chart.

It is recommended that the `LotSizeMultiplier` and `TakeProfitMultiplier` settings are not modified as they provide the Martingale functionality, however the ability to change them has been added so the user can adjust at their discretion.

* `OriginalOrderLotSize`

    This is the Lot size of the first pair of orders created. Make sure this is set small enough to allow further, larger lots to be placed in martingale fashion.

* `OriginalOrderTakeProfit`

    This is the size of Take Profit for the first pair of orders created. This value is in Metatrader 'Points' for EU, there are 10 points per pip.

* `OriginalOrderAtNewBarOnly`

    When this is set to true, the first pair of orders will only be opened when a new bar starts. When set to false the EA will try to open the first pair of orders as soon as possible.

* `LotSizeMultiplier`

    When further orders are placed in a martingale fashion. The subsequent orders Lot Sizes are multiplied by this value.

* `TakeProfitMultiplier`

    When further orders are placed in a martingale fashion. The subsequent orders Take Profit targets are multiplied by this value.

* `AllowedSlippage`

    The amount of slippage allowed when placing a new order

* `KeepTakeProfitOnLargestOrderOnly`

    When a new order is created in a martingale situation, the TakeProfit from any previous orders will be removed, so the current run of open orders can only be ended when the largest order hits profit targets. This attempts to stop issues of lost profits in range-bound markets by older order TakeProfit levels being hit. This defaults to true.

* `OnlyOpenTradesOnWeekdays`

    Depending on locations/timezones, dome trading servers will open on Sunday evenings for a few hours, this can cause orders to be places a couple of hours apart when Monday arrives shortly after. This setting can be used to define if trades should be opened Monday-Friday only. This defaults to true.

## Currency Pairs and settings

These are suggestions only and should not be used in place of proper back and forward testing. `OriginalOrderLotSize` will be dependent on your account size and leverage.

| Pair   | Timeframe | OriginalOrderTakeProfit |
|--------|:---------:|:------------------------|
| EURUSD | D1        | 1000                    |
| EURUSD | H1        | 100                     |

## Notes and suggestions

* Always run this EA within a test or demo environment before running on a live account.
* There is no gaurantee that this will be profitable. It is possible to lose more money than invested.

## Bugs

* Please report any bugs found including the version number being used and a full description of the issue including a copy of the logs from the Experts tab when the issue occured.

## License

This is distributed under GPL v3.0. See the included `LICENSE` file.

## Notice and Warranty

The software is provided "as is", without warranty of any kind, express or implied.

## Website

https://gitlab.com/alasdairkeyes/mt4-fauxcharger
